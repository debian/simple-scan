# Persian translation for simple-scan.
# Copyright (C) 2017 simple-scan's COPYRIGHT HOLDER
# This file is distributed under the same license as the simple-scan package.
# Goudarz Jafari <goudarz.jafari@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: simple-scan gnome-3-26\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=simple-"
"scan&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2017-09-11 03:50+0000\n"
"PO-Revision-Date: 2017-09-22 17:00+0330\n"
"Language-Team: Persian <fa@li.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"Last-Translator: Goudarz Jafari <goudarz.jafari@gmail.com>\n"
"X-Generator: Poedit 2.0.3\n"

#: data/org.gnome.SimpleScan.gschema.xml:11
msgid "Device to scan from"
msgstr "دستگاه برای اسکن کردن از"

#: data/org.gnome.SimpleScan.gschema.xml:12
msgid "SANE device to acquire images from."
msgstr "دستگاه SANE برای دریافت تصاویر از طریق آن."

#: data/org.gnome.SimpleScan.gschema.xml:20
msgid "Type of document being scanned"
msgstr "نوع نوشتار در حال اسکن"

#: data/org.gnome.SimpleScan.gschema.xml:21
msgid ""
"Type of document being scanned. This setting decides on the scan resolution, "
"colors and post-processing."
msgstr ""
"نوع نوشتار درحال اسکن شدن. این تنظیمات بر روی وضوح اسکن، رنگ‌ها و پردازش پس از "
"اسکن اعمال می‌شود."

#: data/org.gnome.SimpleScan.gschema.xml:25
msgid "Width of paper in tenths of a mm"
msgstr "عرض کاغذ در دهم میلی‌متر"

#: data/org.gnome.SimpleScan.gschema.xml:26
msgid ""
"The width of the paper in tenths of a mm (or 0 for automatic paper detection)."
msgstr "عرض کاغذ در دهم میلی‌متر (یا 0 برای تشخیص اتوماتیک کاغذ)."

#: data/org.gnome.SimpleScan.gschema.xml:30
msgid "Height of paper in tenths of a mm"
msgstr "ارتفاع کاغذ در دهم میلی‌متر"

#: data/org.gnome.SimpleScan.gschema.xml:31
msgid ""
"The height of the paper in tenths of a mm (or 0 for automatic paper detection)."
msgstr "ارتفاع کاغذ در دهم میلی‌متر (یا 0 برای تشخیص اتوماتیک کاغذ)."

#: data/org.gnome.SimpleScan.gschema.xml:35
msgid "Brightness of scan"
msgstr "روشنایی اسکن"

#: data/org.gnome.SimpleScan.gschema.xml:36
msgid "The brightness adjustment from -100 to 100 (0 being none)."
msgstr "تنظیم روشنایی از -۱۰۰ تا ۱۰۰ (0 هیچ‌کدام)."

#: data/org.gnome.SimpleScan.gschema.xml:40
msgid "Contrast of scan"
msgstr "سایه روشن اسکن"

#: data/org.gnome.SimpleScan.gschema.xml:41
msgid "The contrast adjustment from -100 to 100 (0 being none)."
msgstr "تنظیم کنتراست از -۱۰۰ تا ۱۰۰ (۰ هیچ کدام)"

#: data/org.gnome.SimpleScan.gschema.xml:45
msgid "Resolution for text scans"
msgstr "تفکیک‌پذیری برای اسکن متن"

#: data/org.gnome.SimpleScan.gschema.xml:46
msgid "The resolution in dots-per-inch to use when scanning text."
msgstr "تفکیک‌پذیری نقطه در هر اینچ برای استفاده در اسکن متن."

#: data/org.gnome.SimpleScan.gschema.xml:50
msgid "Resolution for photo scans"
msgstr "تفکیک‌پذیری برای اسکن عکس"

#: data/org.gnome.SimpleScan.gschema.xml:51
msgid "The resolution in dots-per-inch to use when scanning photos."
msgstr "تفکیک‌پذیری نقطه در هر اینچ برای استفاده در هنگام اسکن تصاویر."

#: data/org.gnome.SimpleScan.gschema.xml:55
msgid "Page side to scan"
msgstr "سمتِ صفحه برای اسکن"

#: data/org.gnome.SimpleScan.gschema.xml:56
msgid "The page side to scan."
msgstr "سمتِ صفحه‌ای که باید اسکن شود."

#: data/org.gnome.SimpleScan.gschema.xml:60
msgid "Directory to save files to"
msgstr "شاخه مربوط به ذخیرهٔ پرونده‌ها"

#: data/org.gnome.SimpleScan.gschema.xml:61
msgid ""
"The directory to save files to. Defaults to the documents directory if unset."
msgstr ""
"شاخه‌ای که پرونده‌ها در آن ذخیره می‌شوند. اگر تنظیم نشود، پیش‌فرض شاخهٔ نوشتارها."

#: data/org.gnome.SimpleScan.gschema.xml:66
msgid "Quality value to use for JPEG compression"
msgstr "میزان کیفیت برای استفاده در هنگام فشرده‌سازی JPEG"

#: data/org.gnome.SimpleScan.gschema.xml:67
msgid "Quality value to use for JPEG compression."
msgstr "میزان کیفیت برای استفاده در هنگام فشرده‌سازی JPEG."

#: data/org.gnome.SimpleScan.gschema.xml:72
msgid "Delay in millisecond between pages"
msgstr "تاخیر زمانی بین صفحات به میلی ثانیه"

#: data/org.gnome.SimpleScan.gschema.xml:73
msgid "Delay in millisecond between pages."
msgstr "تاخیر زمانی بین صفحات به میلی ثانیه."

#. Title of scan window
#. Set HeaderBar title here because Glade doesn't keep it translated
#. https://bugzilla.gnome.org/show_bug.cgi?id=782753
#. Title of scan window
#: data/simple-scan.appdata.xml.in:6 data/simple-scan.desktop.in:3
#: src/app-window.ui:76 src/app-window.vala:1560
msgid "Simple Scan"
msgstr "اسکن ساده"

#: data/simple-scan.appdata.xml.in:7 data/simple-scan.desktop.in:5
msgid "Scan Documents"
msgstr "اسکن پرونده‌ها"

#: data/simple-scan.appdata.xml.in:9
msgid ""
"A really easy way to scan both documents and photos. You can crop out the bad "
"parts of a photo and rotate it if it is the wrong way round. You can print "
"your scans, export them to pdf, or save them in a range of image formats."
msgstr ""
"یک راه ساده برای اسکن اسناد و تصاویر. شما میتوانید قسمت‌های نامتناسب را برش "
"دهید و بچرخانید. می‌توانید اسکن‌های خود را چاپ کنید، یا آنها را به pdf منتقل "
"کنید، یا در بازه‌ای در قالب‌های مختلف تصویر، ذخیره کنید."

#: data/simple-scan.appdata.xml.in:14
msgid "Simple Scan uses the SANE framework to support most existing scanners."
msgstr ""
"«اسکن ساده» از چهارچوب SANE جهت پشتیبانی از اسکنرهای موجود استفاده می‌کند."

#: data/simple-scan.desktop.in:4
msgid "Document Scanner"
msgstr "اسکنر پرونده"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/simple-scan.desktop.in:7
msgid "scan;scanner;flatbed;adf;"
msgstr "scan;scanner;flatbed;adf;اسکن;اسکنر;"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/simple-scan.desktop.in:10
msgid "scanner"
msgstr "scanner"

#. Toolbar scan menu item to scan a single page from the scanner
#. Scan menu item to scan a single page from the scanner
#. Toolbar scan menu item to scan a single page from the scanner
#: src/app-window.ui:22 src/app-window.ui:123 src/app-window.ui:820
msgid "Single _Page"
msgstr "تک _صفحه‌ای"

#. Toolbar scan menu item to scan all pages from a document feeder
#. Scan menu item to scan all pages from a document feeder
#. Toolbar scan menu item to scan all pages from a document feeder
#: src/app-window.ui:31 src/app-window.ui:133 src/app-window.ui:829
msgid "All Pages From _Feeder"
msgstr "تمام صفحات از _منبع تغذیه"

#. Toolbar scan menu item to scan continuously from the flatbed
#. Scan menu item to scan continuously from the flatbed
#. Toolbar scan menu item to scan continuously from the flatbed
#: src/app-window.ui:40 src/app-window.ui:143 src/app-window.ui:838
msgid "_Multiple Pages From Flatbed"
msgstr "_چند صفحه از Flatbed"

#: src/app-window.ui:55 src/app-window.ui:170 src/app-window.ui:853
msgid "Text"
msgstr "متن"

#: src/app-window.ui:65 src/app-window.ui:180 src/app-window.ui:863
msgid "Photo"
msgstr "عکس"

#. Label on document menu (contains actions for this document, e.g. save, print)
#: src/app-window.ui:91
msgid "_Document"
msgstr "_سند"

#. Scan menu item
#: src/app-window.ui:110
msgid "Sc_an"
msgstr "ا_سکن"

#. Menu entry to stop current scan
#: src/app-window.ui:154
msgid "_Stop Scan"
msgstr "_توقف اسکن"

#. Menu item to reorder pages
#. Title of dialog to reorder pages
#: src/app-window.ui:196 src/app-window.vala:967 src/app-window.vala:1590
msgid "Reorder Pages"
msgstr "جابجایی صفحات"

#. Label on email menu item
#: src/app-window.ui:215
msgid "_Email"
msgstr "_پست‌الکترونیکی"

#. Page menu (contains action for each page, e.g. delete, crop)
#: src/app-window.ui:280
msgid "_Page"
msgstr "_صفحه"

#. Menu item to rotate page to left (anti-clockwise)
#: src/app-window.ui:290
msgid "Rotate _Left"
msgstr "چرخش به _چپ"

#. Menu item to rotate page to right (clockwise)
#: src/app-window.ui:300
msgid "Rotate _Right"
msgstr "چرخش به _راست"

#. Label for page crop submenu
#: src/app-window.ui:310
msgid "_Crop"
msgstr "_برش"

#. Radio button for no crop
#: src/app-window.ui:320
msgid "_None"
msgstr "_هیچ‌کدام"

#. Radio button for cropping page to A4 size
#: src/app-window.ui:331
msgid "A_4"
msgstr "A_4"

#. Radio button for cropping page to A5 size
#: src/app-window.ui:342
msgid "A_5"
msgstr "A_5"

#. Radio button for cropping page to A6 size
#: src/app-window.ui:353
msgid "A_6"
msgstr "A_6"

#. Radio button for cropping page to US letter size
#: src/app-window.ui:364
msgid "_Letter"
msgstr "_نامه"

#. Radio button for cropping to page to US legal size
#: src/app-window.ui:375
msgid "Le_gal"
msgstr "_قانونی"

#. Radio button for cropping page to 4x6 inch
#: src/app-window.ui:386
msgid "4×6"
msgstr "۶×۴"

#. Radio button for cropping to custom page size
#: src/app-window.ui:397
msgid "_Custom"
msgstr "_سفارشی"

#. Menu item to rotate the crop area
#: src/app-window.ui:415
msgid "_Rotate Crop"
msgstr "_چرخاندن برش"

#. Menu item to move the selected page to the left
#: src/app-window.ui:428
msgid "Move Left"
msgstr "جابه‌جایی به چپ"

#. Menu item to move the selected page to the right
#: src/app-window.ui:437
msgid "Move Right"
msgstr "جابه‌جایی به راست"

#. Label on help menu
#: src/app-window.ui:474
msgid "_Help"
msgstr "_راهنما"

#. Help|Contents menu
#: src/app-window.ui:482
msgid "_Contents"
msgstr "_فهرست"

#. Tooltip for scan toolbar button
#: src/app-window.ui:520 src/app-window.ui:720
msgid "Scan a single page from the scanner"
msgstr "اسکن یک صفحه از طریق اسکنر"

#. Label on scan toolbar item
#: src/app-window.ui:522 src/app-window.ui:716
msgid "Scan"
msgstr "اسکن"

#. Tooltip for save toolbar button
#: src/app-window.ui:538 src/app-window.ui:787
msgid "Save document to a file"
msgstr "ذخیره سند در پرونده"

#: src/app-window.ui:540
msgid "Save"
msgstr "ذخیره"

#. Tooltip for stop button
#: src/app-window.ui:555 src/app-window.ui:700
msgid "Stop the current scan"
msgstr "توقف اسکن فعلی"

#: src/app-window.ui:556 src/app-window.ui:697
msgid "Stop"
msgstr "توقف"

#. Label shown when searching for scanners
#: src/app-window.ui:611
msgid "Searching for Scanners…"
msgstr "در حال جست‌وجو برای اسکنرها…"

#: src/app-window.vala:237 src/app-window.vala:1380
msgid "_Close"
msgstr "_بستن"

#. Label in authorization dialog.  “%s” is replaced with the name of the resource requesting authorization
#: src/app-window.vala:246
#, c-format
msgid "Username and password required to access “%s”"
msgstr "برای دسترسی به (%s) نیاز به نام کاربری و رمز عبور دارید"

#. Label shown when detected a scanner
#: src/app-window.vala:266 src/app-window.vala:584
msgid "Ready to Scan"
msgstr "آماده برای اسکن"

#. Warning displayed when no drivers are installed but a compatible scanner is detected
#: src/app-window.vala:273
msgid "Additional software needed"
msgstr "نرم افزار اضافی مورد نیاز است"

#. Instructions to install driver software
#: src/app-window.vala:275
msgid ""
"You need to <a href=\"install-firmware\">install driver software</a> for your "
"scanner."
msgstr ""
"لازم است <a href=\"install-firmware\">نرم‌افزازِ راه انداز</a> را برای اسکنر خود "
"نصب کنید."

#. Warning displayed when no scanners are detected
#: src/app-window.vala:281
msgid "No scanners detected"
msgstr "اسکنری پیدا نشد"

#. Hint to user on why there are no scanners detected
#: src/app-window.vala:283
msgid "Please check your scanner is connected and powered on"
msgstr "لطفا بررسی کنید که اسکنر شما متصل و روشن باشد"

#. Save dialog: Dialog title
#: src/app-window.vala:298
msgid "Save As…"
msgstr "ذخیره به عنوان…"

#: src/app-window.vala:301 src/app-window.vala:471 src/app-window.vala:552
msgid "_Cancel"
msgstr "_لغو"

#: src/app-window.vala:302 src/app-window.vala:553
msgid "_Save"
msgstr "_ذخیره"

#. Default filename to use when saving document
#: src/app-window.vala:310
msgid "Scanned Document.pdf"
msgstr "Scanned Document.pdf"

#. Save dialog: Filter name to show only supported image files
#: src/app-window.vala:316
msgid "Image Files"
msgstr "پرونده‌های تصاویر"

#. Save dialog: Filter name to show all files
#: src/app-window.vala:326
msgid "All Files"
msgstr "تمام پرونده‌ها"

#. Save dialog: Label for saving in PDF format
#: src/app-window.vala:335
msgid "PDF (multi-page document)"
msgstr "PDF (سند چند صفحه‌ای)"

#. Save dialog: Label for saving in JPEG format
#: src/app-window.vala:341
msgid "JPEG (compressed)"
msgstr "JPEG (فشرده)"

#. Save dialog: Label for saving in PNG format
#: src/app-window.vala:347
msgid "PNG (lossless)"
msgstr "PNG (بدون کاهش کیفیت)"

#. Save dialog: Label for sabing in WEBP format
#: src/app-window.vala:354
msgid "WebP (compressed)"
msgstr "WebP (فشرده)"

#. Label in save dialog beside combo box to choose file format (PDF, JPEG, PNG, WEBP)
#: src/app-window.vala:364
msgid "File format:"
msgstr "قالب پرونده:"

#. Label in save dialog beside compression slider
#: src/app-window.vala:376
msgid "Compression:"
msgstr "فشرده‌سازی:"

#. Contents of dialog that shows if saving would overwrite and existing file. %s is replaced with the name of the file.
#: src/app-window.vala:470
#, c-format
msgid "A file named “%s” already exists.  Do you want to replace it?"
msgstr "پرونده‌ای از پیش با نام «%s» ذخیره شده. می‌خواهید آن را جای‌گزین کنید؟"

#. Button in dialog that shows if saving would overwrite and existing file. Clicking the button allows simple-scan to overwrite the file.
#: src/app-window.vala:473
msgid "_Replace"
msgstr "_جایگزینی"

#: src/app-window.vala:512
msgid "Saving"
msgstr "درحال ذخیره"

#. Title of error dialog when save failed
#: src/app-window.vala:527
msgid "Failed to save file"
msgstr "ذخیره پرونده شکست خورد"

#. Text in dialog warning when a document is about to be lost
#: src/app-window.vala:550
msgid "If you don’t save, changes will be permanently lost."
msgstr "اگر پرونده را ذخیره نکنید تغییرات برای همیشه از بین میرود."

#. Text in dialog warning when a document is about to be lost
#: src/app-window.vala:591
msgid "Save current document?"
msgstr "ذخیره سند جاری؟"

#. Button in dialog to create new document and discard unsaved document
#: src/app-window.vala:593
msgid "Discard Changes"
msgstr "نادیده گرفتن تغییرات"

#. Label shown when scan started
#: src/app-window.vala:692
msgid "Contacting scanner…"
msgstr "در حال برقراری ارتباط با اسکنر…"

#. Error message display when unable to save image for preview
#: src/app-window.vala:800
msgid "Unable to save image for preview"
msgstr "نمی‌توان پرونده را برای پیش‌نمایش ذخیره کرد"

#. Error message display when unable to preview image
#: src/app-window.vala:812
msgid "Unable to open image preview application"
msgstr "نمی‌توان برنامه پیش‌نمایش تصویر را باز کرد"

#. Label on button for combining sides in reordering dialog
#: src/app-window.vala:990
msgid "Combine sides"
msgstr "ترکیبِ طرف‌ها"

#. Label on button for combining sides in reverse order in reordering dialog
#: src/app-window.vala:1000
msgid "Combine sides (reverse)"
msgstr "ترکیب دو طرف (معکوس)"

#. Label on button for reversing in reordering dialog
#: src/app-window.vala:1010
msgid "Reverse"
msgstr "معکوس"

#. Label on button for cancelling page reordering dialog
#: src/app-window.vala:1020
msgid "Keep unchanged"
msgstr "نگه‌داشتن بدون تغییر"

#. Error message displayed when unable to launch help browser
#: src/app-window.vala:1250
msgid "Unable to open help file"
msgstr "نمیتوان فایل راهنما را باز کرد"

#. The license this software is under (GPL3+)
#: src/app-window.vala:1271
msgid ""
"This program is free software: you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation, either version 3 of the License, or\n"
"(at your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License\n"
"along with this program.  If not, see <http://www.gnu.org/licenses/>."
msgstr ""
"این نرم‌افزار، یک نرم‌افزار آزاد است؛ شما می‌توانید آن را تحت شرایط اجازه‌نامه‌ی "
"همگانی عمومی گنو\n"
"که بنیاد نرم‌افزارهای آزاد منتشر کرده است،‌ تغییر دهید یا دوباره توزیع کنید. یا "
"نسخه‌ی ۲ اجازه‌نامه\n"
"یا (به اختیار خودتان) هر نسخه‌ی بالاتر دیگری.\n"
"\n"
"این برنامه با این امید توزیع شده است که به درد بخور باشد، اما بدون هر گونه "
"ضمانتی؛ حتی بدون\n"
"ضمانت ضمنی قابل فروش بودن یا مناسب بودن برای یک هدف مشخص. برای جزئیات بیشتر "
"به\n"
"اجازه‌نامه‌ی همگانی عمومی گنو را ببینید.\n"
"\n"
"شما باید به همراه این برنامه رونوشتی از اجازه‌نامه‌ی عمومی همگانی گنو را دریافت "
"می‌کردید،\n"
"اگر این‌گونه نیست، «http://www.gnu.org/licenses» را ببینید."

#. Title of about dialog
#: src/app-window.vala:1274
msgid "About Simple Scan"
msgstr "دربارهٔ «اسکن ساده»"

#. Description of program
#: src/app-window.vala:1277
msgid "Simple document scanning tool"
msgstr "یک اسکنر ساده برای اسناد"

#: src/app-window.vala:1286
msgid "translator-credits"
msgstr ""
"Arash Mousavi <mousavi.arash@gmail.com>\n"
"goudarz jafari <goudarz.jafari@gmail.com>\n"
"ssshojaei <shojaeisaleh@gmail.com>"

#. Text in dialog warning when a document is about to be lost
#: src/app-window.vala:1308
msgid "Save document before quitting?"
msgstr "ذخیره پرونده قبل از خروج؟"

#. Text in dialog warning when a document is about to be lost
#: src/app-window.vala:1310
msgid "Quit without Saving"
msgstr "خروج بدون ذخیره‌سازی"

#. Message to indicate a Brother scanner has been detected
#: src/app-window.vala:1357
msgid "You appear to have a Brother scanner."
msgstr "ظاهرا شما یک اسکنر Brother دارید."

#. Instructions on how to install Brother scanner drivers
#: src/app-window.vala:1359
msgid ""
"Drivers for this are available on the <a href=\"http://support.brother.com"
"\">Brother website</a>."
msgstr ""
"راه اندازها برای این دستگاه را میتوانید از <a href=\"http://support.brother.com"
"\">وب‌سایت Brother</a> پیدا کنید."

#. Message to indicate a Samsung scanner has been detected
#: src/app-window.vala:1363
msgid "You appear to have a Samsung scanner."
msgstr "ظاهرا شما یک اسکنر سامسونگ دارید."

#. Instructions on how to install Samsung scanner drivers
#: src/app-window.vala:1365
msgid ""
"Drivers for this are available on the <a href=\"http://samsung.com/support"
"\">Samsung website</a>."
msgstr ""
"راه اندازها برای این دستگاه را میتوانید از <a href=\"http://samsung.com/support"
"\">وب‌سایت سامسونگ</a> پیدا کنید."

#. Message to indicate a HP scanner has been detected
#: src/app-window.vala:1369
msgid "You appear to have an HP scanner."
msgstr "ظاهرا شما یک اسکنر HP دارید."

#. Message to indicate an Epson scanner has been detected
#: src/app-window.vala:1374
msgid "You appear to have an Epson scanner."
msgstr "شما ظاهرا یک اسکنر Epson دارید."

#. Instructions on how to install Epson scanner drivers
#: src/app-window.vala:1376
msgid ""
"Drivers for this are available on the <a href=\"http://support.epson.com"
"\">Epson website</a>."
msgstr ""
"راه اندازها برای این دستگاه را میتوانید از <a href=\"http://support.epson.com"
"\">وب‌سایت Epson</a> پیدا کنید."

#. Title of dialog giving instructions on how to install drivers
#: src/app-window.vala:1380
msgid "Install drivers"
msgstr "نصب راه‌اندازها"

#. Message in driver install dialog
#: src/app-window.vala:1411
msgid "Once installed you will need to restart Simple Scan."
msgstr "پس از نصب شما باید برنامه «اسکن ساده» را دوباره باز کنید."

#. Label shown while installing drivers
#: src/app-window.vala:1423
msgid "Installing drivers…"
msgstr "در حال نصب راه‌اندازها…"

#. Label shown once drivers successfully installed
#: src/app-window.vala:1431
msgid "Drivers installed successfully!"
msgstr "راه‌اندازها با موفقیت نصب شدند!"

#. Label shown if failed to install drivers
#: src/app-window.vala:1441
#, c-format
msgid "Failed to install drivers (error code %d)."
msgstr "نصب راه‌انداز شکست خورد (خطا در کد %Id)"

#. Label shown if failed to install drivers
#: src/app-window.vala:1447
msgid "Failed to install drivers."
msgstr "خطا در نصب راه‌انداز."

#. Label shown to prompt user to install packages (when PackageKit not available)
#: src/app-window.vala:1454
#, c-format
msgid "You need to install the %s package."
msgid_plural "You need to install the %s packages."
msgstr[0] "لازم است بستهٔ %s را نصب کنید."

#. Title of preferences dialog
#: src/app-window.vala:1568 src/app-window.vala:1591 src/preferences-dialog.ui:53
msgid "Preferences"
msgstr "تنظیمات"

#: src/app-window.vala:1572
msgid "Keyboard Shortcuts"
msgstr "میانبرهای کیبورد"

#: src/app-window.vala:1573
msgid "Help"
msgstr "راهنما"

#: src/app-window.vala:1574
msgid "About"
msgstr "درباره"

#: src/app-window.vala:1575
msgid "Quit"
msgstr "خروج"

#: src/app-window.vala:1589
msgid "Email"
msgstr "پست‌الکترونیکی"

#. Populate ActionBar (not supported in Glade)
#. https://bugzilla.gnome.org/show_bug.cgi?id=769966
#. Label on new document button
#: src/app-window.vala:1599
msgid "Start Again…"
msgstr "شروع دوباره…"

#. Tooltip for rotate left (counter-clockwise) button
#: src/app-window.vala:1616
msgid "Rotate the page to the left (counter-clockwise)"
msgstr "چرخاندن صفحه به چپ (خلاف جهت عقربه‌های ساعت)"

#. Tooltip for rotate right (clockwise) button
#: src/app-window.vala:1623
msgid "Rotate the page to the right (clockwise)"
msgstr "چرخاندن صفحه به راست (در جهت عقربه‌های ساعت)"

#. Tooltip for crop button
#: src/app-window.vala:1633
msgid "Crop the selected page"
msgstr "برش صفحه انتخاب شده"

#. Tooltip for delete button
#: src/app-window.vala:1649
msgid "Delete the selected page"
msgstr "حذف صفحه انتخاب شده"

#. Text of button for cancelling save
#: src/app-window.vala:1814
msgid "Cancel"
msgstr "لغو"

#. Button to submit authorization dialog
#: src/authorize-dialog.ui:25
msgid "_Authorize"
msgstr "_تایید مجوز"

#. Label beside username entry
#: src/authorize-dialog.ui:99
msgid "_Username for resource:"
msgstr "_نام کاربری برای منبع:"

#. Label beside password entry
#: src/authorize-dialog.ui:112
msgid "_Password:"
msgstr "_گذرواژه:"

#: src/book.vala:331 src/book.vala:337
#, c-format
msgid "Unable to encode page %i"
msgstr "خطا در رمزگذاری صفحه %Ii"

#: src/help-overlay.ui:12
msgctxt "shortcut window"
msgid "Scanning"
msgstr "در حال اسکن"

#: src/help-overlay.ui:17
msgctxt "shortcut window"
msgid "Scan a single page"
msgstr "اسکنِ یک صفحه"

#: src/help-overlay.ui:24
msgctxt "shortcut window"
msgid "Scan all pages from document feeder"
msgstr "اسکن کردن تمام صفحات در منبع تغذیه سند"

#: src/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Scan continuously from a flatbed scanner"
msgstr "اسکن مداوم از یک اسکنر flatbed"

#: src/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Stop scan in progress"
msgstr "متوقف کردن اسکن جاری"

#: src/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Document Modification"
msgstr "اصلاح سند"

#: src/help-overlay.ui:51
msgctxt "shortcut window"
msgid "Move page left"
msgstr "جابه‌جایی صفحه به چپ"

#: src/help-overlay.ui:58
msgctxt "shortcut window"
msgid "Move page right"
msgstr "جابه‌جایی صفحه به راست"

#: src/help-overlay.ui:65
msgctxt "shortcut window"
msgid "Rotate page to the left (anti-clockwise)"
msgstr "چرخاندن صفحه به چپ (خلاف جهت عقربه‌های ساعت)"

#: src/help-overlay.ui:72
msgctxt "shortcut window"
msgid "Rotate page to the right (clockwise)"
msgstr "چرخاندن صفحه به راست (در جهت عقربه‌های ساعت)"

#: src/help-overlay.ui:79
msgctxt "shortcut window"
msgid "Delete page"
msgstr "حذف صفحه"

#: src/help-overlay.ui:87
msgctxt "shortcut window"
msgid "Document Management"
msgstr "مدیریت سندها"

#: src/help-overlay.ui:92
msgctxt "shortcut window"
msgid "Start new document"
msgstr "شروع اسکن سند جدید"

#: src/help-overlay.ui:99
msgctxt "shortcut window"
msgid "Save scanned document"
msgstr "ذخیره سند اسکن شده"

#: src/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Email scanned document"
msgstr "پست سند اسکن شده"

#: src/help-overlay.ui:113
msgctxt "shortcut window"
msgid "Print scanned document"
msgstr "چاپ سند اسکن شده"

#: src/help-overlay.ui:120
msgctxt "shortcut window"
msgid "Copy current page to clipboard"
msgstr "رونوشت از این صفحه در تخته‌گیره"

#. Label beside scan source combo box
#: src/preferences-dialog.ui:113
msgid "_Scanner"
msgstr "_اسکنر"

#. Label beside scan side combo box
#: src/preferences-dialog.ui:143
msgid "Scan Sides"
msgstr "اسکن سمت‌های مختلف"

#. Label beside page size combo box
#: src/preferences-dialog.ui:160
msgid "Page Size"
msgstr "اندازه صفحه"

#. Preferences Dialog: Toggle button to select scanning on front side of a page
#: src/preferences-dialog.ui:191
msgid "Front"
msgstr "جلو"

#. Preferences Dialog: Toggle button to select scanning on the back side of a page
#: src/preferences-dialog.ui:206
msgid "Back"
msgstr "عقب"

#. Preferences Dialog: Toggle button to select scanning on both sides of a page
#: src/preferences-dialog.ui:221
msgid "Both"
msgstr "هر دو"

#. Label beside page delay scale
#: src/preferences-dialog.ui:260
msgid "Delay"
msgstr "تاخیر"

#. Preferences dialog: Label above settings for scanning multiple pages from a flatbed
#: src/preferences-dialog.ui:276
msgid "Multiple pages from flatbed"
msgstr "صفحات چندگانه از flatbed"

#. Preferences Dialog: Toggle button to select scanning on front side of a page
#: src/preferences-dialog.ui:295
msgid "3"
msgstr "۳"

#. Preferences Dialog: Toggle button to select scanning on front side of a page
#: src/preferences-dialog.ui:310
msgid "5"
msgstr "۵"

#. Preferences Dialog: Toggle button to select scanning on front side of a page
#: src/preferences-dialog.ui:326
msgid "7"
msgstr "۷"

#. Preferences Dialog: Toggle button to select scanning on front side of a page
#: src/preferences-dialog.ui:342
msgid "10"
msgstr "۱۰"

#. Preferences Dialog: Toggle button to select scanning on front side of a page
#: src/preferences-dialog.ui:358
msgid "15"
msgstr "۱۵"

#. Label after page delay radio buttons
#: src/preferences-dialog.ui:385
msgid "Seconds"
msgstr "ثانیه"

#. Preferences Dialog: Tab label for scanning settings
#: src/preferences-dialog.ui:413
msgid "Scanning"
msgstr "در حال اسکن"

#. Label beside scan source combo box
#: src/preferences-dialog.ui:436
msgid "_Text Resolution"
msgstr "_تفکیک‌پذیری متن"

#. Label beside scan source combo box
#: src/preferences-dialog.ui:452
msgid "_Photo Resolution"
msgstr "_تفکیک‌پذیری تصویر"

#. Label beside brightness scale
#: src/preferences-dialog.ui:505
msgid "Brightness"
msgstr "روشنایی"

#. Label beside contrast scale
#: src/preferences-dialog.ui:521
msgid "Contrast"
msgstr "سایه‌روشن"

#. Preferences Dialog: Tab for quality settings
#: src/preferences-dialog.ui:576
msgid "Quality"
msgstr "کیفیت"

#. Combo box value for automatic paper size
#: src/preferences-dialog.vala:77
msgid "Automatic"
msgstr "خودکار"

#: src/preferences-dialog.vala:116
msgid "Darker"
msgstr "تیره‌تر"

#: src/preferences-dialog.vala:118
msgid "Lighter"
msgstr "روشن‌تر"

#: src/preferences-dialog.vala:126
msgid "Less"
msgstr "کمتر"

#: src/preferences-dialog.vala:128
msgid "More"
msgstr "بیشتر"

#. Preferences dialog: Label for default resolution in resolution list
#: src/preferences-dialog.vala:440
#, c-format
msgid "%d dpi (default)"
msgstr "%Id dpi (پیش فرض)"

#. Preferences dialog: Label for minimum resolution in resolution list
#: src/preferences-dialog.vala:443
#, c-format
msgid "%d dpi (draft)"
msgstr "%Id dpi (پیش نویس)"

#. Preferences dialog: Label for maximum resolution in resolution list
#: src/preferences-dialog.vala:446
#, c-format
msgid "%d dpi (high resolution)"
msgstr "%Id dpi (بالاترین تفکیک‌پذیری)"

#. Preferences dialog: Label for resolution value in resolution list (dpi = dots per inch)
#: src/preferences-dialog.vala:449
#, c-format
msgid "%d dpi"
msgstr "%Id dpi"

#. Error displayed when no scanners to scan with
#: src/scanner.vala:844
msgid "No scanners available.  Please connect a scanner."
msgstr "اسکنری موجود نیست. لطفا یک اسکنر را متصل کنید."

#. Error displayed when cannot connect to scanner
#: src/scanner.vala:874
msgid "Unable to connect to scanner"
msgstr "امکان برقراری ارتباط با اسکنر نبود"

#. Error display when unable to start scan
#: src/scanner.vala:1227
msgid "Unable to start scan"
msgstr "امکان شروع اسکن نبود"

#. Error displayed when communication with scanner broken
#: src/scanner.vala:1240 src/scanner.vala:1340
msgid "Error communicating with scanner"
msgstr "خطا در هنگام بر قراری ارتباط با اسکنر"

#. Help string for command line --version flag
#: src/simple-scan.vala:21
msgid "Show release version"
msgstr "نمایش نسخهٔ انتشار"

#. Help string for command line --debug flag
#: src/simple-scan.vala:24
msgid "Print debugging messages"
msgstr "نمایش پیام‌های اشکال‌زدایی"

#: src/simple-scan.vala:26
msgid "Fix PDF files generated with older versions of Simple Scan"
msgstr "فایل های PDF را با نسخه‌های قدیمی‌تر «اسکن ساده» ایجاد کنید"

#. Title of error dialog when scan failed
#: src/simple-scan.vala:392
msgid "Failed to scan"
msgstr "اسکن شکست خورد"

#. Attempt to inhibit the screensaver when scanning
#: src/simple-scan.vala:407
msgid "Scan in progress"
msgstr "در حال انجام است"

#. Arguments and description for --help text
#: src/simple-scan.vala:584
msgid "[DEVICE…] — Scanning utility"
msgstr "[DEVICE…] — ابزار اسکن"

#. Text printed out when an unknown command-line argument provided
#: src/simple-scan.vala:595
#, c-format
msgid "Run “%s --help” to see a full list of available command line options."
msgstr "برای مشاهده راهنمای خط فرمان (%s --help) را وارد کنید."
